const express = require('express');
const morgan = require('morgan');
const cors = require('morgan');
const mustache = require('mustache-express');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(morgan('dev'));
app.use(cors({ origin: '*' }));

app.engine('.mustache', mustache());
app.set('view engine', 'mustache');

app.get('/std', (req, res) => res.render('std', {
  shipAddress: {
    name: 'Ship address name',
    address1: 'Address 1',
    address2: 'Address 2',
  },
}));

app.listen(4001);
